#
# ~/.customrc
#

# If not running interactively, do not do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'

unset use_color safe_term match_lhs sh

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# custom aliases
alias iwant='sudo pacman -S'
alias ihate='sudo pacman -R'
alias ifind='pacman -Q'
alias ineed='pacman -U'

alias edit="mousepad $1 &"
alias nuke="rm -rf"

alias df="df -h"

alias config='ls ~ -pa | grep -v / | column'

function reload(){
  source /home/$USER/.zshrc
}

alias i3cheatsheet='egrep ^bind ~/.config/i3/config | cut -d '\'' '\'' -f 2- | sed '\''s/ /\t\t\t /'\'' | rofi -dmenu -width 900 '

# utility functions
function hunt(){ ps -xa --format 'command pid' | grep  $1 | column }

function cht(){ curl https://cht.sh/$1 }

# additional lua libraries / luarocks
export LUA_PATH="/usr/share/awesome/lib/?/init.lua"
export LUA_PATH="$LUA_PATH;/usr/share/awesome/lib/?.lua"
export LUA_PATH="$LUA_PATH;;"

export EDITOR=howl

export TERMINAL="alacritty"

if [ -z "$TMUX" ]; then
  tmux new-session
fi
